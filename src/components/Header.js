import React from 'react';

function Header() {
    return (
        <div>
            <br/><br/>
            <h1>Expense Tracker</h1>
        </div>
    );
}

export default Header;